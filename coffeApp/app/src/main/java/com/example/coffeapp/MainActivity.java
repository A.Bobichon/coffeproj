package com.example.coffeapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

class Coffe{
    private Boolean cream;
    private Boolean choco;
    private int quantCream;
    private int quantChoco;
    private int total;

    Coffe(){
        this.cream = false;
        this.choco = false;
        this.quantCream = 0;
        this.quantChoco = 0;
        this.total = 4;
    }

    public Boolean getCream(){
        return this.cream;
    }

    public Boolean getChoco(){
        return this.choco;
    }

    public int getQuantCream(){
        return this.quantCream;
    }

    public int getQuantChoco(){
        return this.quantChoco;
    }

    public int getTotal(){
        return this.total;
    }

    public Coffe setCream(Boolean cream){
        this.cream = cream;
        return this;
    }

    public Coffe setChoco(Boolean choco){
        this.choco = choco;
        return this;
    }

    public Coffe setQuantCream(int QC){
        this.quantCream = QC;
        return this;
    }

    public Coffe setQuantChoco(int QC){
        this.quantChoco = QC;
        return this;
    }

    public Coffe setTotal(){
        this.total += this.getQuantChoco() + this.getQuantCream() * 0.5;
        return this;
    }

    public Coffe AddCream(){
        this.setQuantCream(this.getQuantCream() + 1);
        return this;
    }

    public Coffe AddChoco(){
        this.setQuantChoco(this.getQuantChoco() + 1);
        return this;
    }

    public Coffe SupCream(){
        if (this.getQuantCream() > 0) {
            this.setQuantCream(this.getQuantCream() - 1);
        }
        return this;
    }

    public Coffe SuppChoco(){
        if (this.getQuantChoco() > 0) {
            this.setQuantChoco(this.getQuantChoco() - 1);
        }
        return this;
    }


};


public class MainActivity extends AppCompatActivity {

    private void Hide(Coffe c, View v) {

        ConstraintLayout creamLayout = (ConstraintLayout) findViewById(R.id.CreamQuantity);
        ConstraintLayout chocoLayout = (ConstraintLayout) findViewById(R.id.QuantChocolate);
        ConstraintLayout QuantLayout = (ConstraintLayout) findViewById(R.id.Quantity);

        if (c.getChoco() && !c.getCream()) {
            creamLayout.setVisibility(v.GONE);
            chocoLayout.setVisibility(v.VISIBLE);
            QuantLayout.setMaxHeight(500);
        } else if (!c.getChoco() && c.getCream()) {
            creamLayout.setVisibility(v.VISIBLE);
            chocoLayout.setVisibility(v.GONE);
            QuantLayout.setMaxHeight(500);
        } else {
            creamLayout.setVisibility(v.GONE);
            chocoLayout.setVisibility(v.GONE);
            QuantLayout.setMaxHeight(50);
        }

    };


    private void setOrderSumary(TextView txtAdd, TextView txtToto, TextView QuantTxt, Coffe coffe) {
        if (coffe.getQuantCream() > 0 || coffe.getQuantChoco() > 0) {
            txtAdd.setText("True");
            QuantTxt.setText(Integer.toString(coffe.getQuantCream() + coffe.getQuantChoco()));
            txtToto.setText(Integer.toString(coffe.setTotal().getTotal()));
        } else {
            txtAdd.setText("False");
            txtToto.setText(Integer.toString(coffe.setTotal().getTotal()));
        }

    };


    private void setCheckeBox(CheckBox Fbox, CheckBox Sbox, Coffe coffe, View v, Boolean bool1, Boolean bool2) {
        if (Fbox.isChecked()) {
            Sbox.setChecked(false);

            coffe.setCream(bool1);
            coffe.setChoco(bool2);

            Hide(coffe, v);
        } else {
            Fbox.setChecked(false);
        }
    };

    private void setNumber(Coffe coffe, TextView txt, int n) {
        coffe.getTotal();
        txt.setText(Integer.toString(n));
    };

    private void setNumberNeg(Coffe coffe, TextView txt, int n) {
            coffe.getTotal();
            txt.setText(Integer.toString(n));

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Coffe coffe = new Coffe();

        final CheckBox creamBox = ( CheckBox ) findViewById( R.id.cream );
        final CheckBox chocoBox = ( CheckBox ) findViewById( R.id.choco );

        final Button addCream = ( Button ) findViewById(R.id.addCream);
        final Button supCream = ( Button ) findViewById(R.id.removeCream);
        final Button addChoco = ( Button ) findViewById(R.id.addChoco);
        final Button supChoco = ( Button ) findViewById(R.id.removeChoco);
        final TextView creamN = ( TextView ) findViewById(R.id.QuantCream);
        final TextView chocoN = ( TextView ) findViewById(R.id.QuantChoco);

        final Button orderCM = ( Button ) findViewById(R.id.OrderCM);
        final Button orderCO = ( Button ) findViewById(R.id.OrderCO);

        final TextView creamAdded = ( TextView ) findViewById(R.id.creamTF);
        final TextView chocolateAdded = ( TextView ) findViewById(R.id.chocolateTF);
        final TextView quantity = ( TextView ) findViewById(R.id.quantityAdded);
        final TextView Total = ( TextView ) findViewById(R.id.sum);

        final Button reset = ( Button ) findViewById(R.id.reset);


        // Hide the Quantity layout at the start of the app
        ConstraintLayout creamLayout = ( ConstraintLayout ) findViewById(R.id.CreamQuantity);
        ConstraintLayout chocoLayout = ( ConstraintLayout ) findViewById(R.id.QuantChocolate);
        ConstraintLayout QuantLayout = ( ConstraintLayout ) findViewById(R.id.Quantity);

        creamLayout.setVisibility(View.GONE);
        chocoLayout.setVisibility(View.GONE);
        QuantLayout.setMaxHeight(50);


        // If checked show the options

        creamBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCheckeBox(creamBox,chocoBox, coffe, view, true,false);
            }
        });

        chocoBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCheckeBox(chocoBox,creamBox, coffe, view, false,true);
            }
        });

        // Add Quantity
        addCream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumber(coffe.AddCream(), creamN, coffe.getQuantCream());
            }
        });

        addChoco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumber(coffe.AddChoco(), chocoN, coffe.getQuantChoco());
            }
        });

        supCream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumberNeg(coffe.SupCream(), creamN, coffe.getQuantCream());
            }
        });

        supChoco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumberNeg(coffe.SuppChoco(), chocoN, coffe.getQuantChoco());
            }
        });

        orderCM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setOrderSumary(creamAdded, Total, quantity, coffe);
            }
        });

        orderCO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setOrderSumary(chocolateAdded, Total, quantity,coffe);
            }
        });


        // Reset
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                coffe.setCream(false);
                coffe.setChoco(false);
                coffe.setQuantChoco(0);
                coffe.setQuantCream(0);
                coffe.setTotal();

                Hide(coffe, view);
                creamAdded.setText("");
                chocolateAdded.setText("");
                quantity.setText("");
                Total.setText("");
            }
        });
    };

}